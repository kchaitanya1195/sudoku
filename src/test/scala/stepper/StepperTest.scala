package stepper

import org.scalatest.EitherValues
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.*
import strategize.Utils.{EmptyCellException, InvalidPuzzleException}
import strategize.Stepper
import strategize.Stepper.*
import strategize.Stepper.fail as stepFail

import scala.util.{Failure, Success}

class StepperTest extends AnyWordSpec with Matchers with EitherValues {
  private val testString = "testObj"
  private val intermediateStr = "intermediate"
  private val errorCaught = "errorCaught"
  private val failedObj = EmptyCellException(Seq.empty, 3)

  "StepSolver" must {
    "create end node" in {
      val node = end(testString)

      node.el     mustBe Success(testString)
      node.ops    mustBe empty
      node.result mustBe Success(testString)
      node.resume mustBe Right(Success(testString))
    }
    "create failed node" in {
      val node = stepFail(failedObj)

      node.el     mustBe Failure(EmptyCellException(Seq.empty, 3))
      node.ops    mustBe empty
      node.result mustBe Failure(EmptyCellException(Seq.empty, 3))
      node.resume mustBe Right(Failure(EmptyCellException(Seq.empty, 3)))
    }
    "create map operation over object" in {
      val node = end(testString).map(_.substring(4))

      node.el               mustBe Success(testString)
      node.ops.size         mustBe 1
      node.ops.head         mustBe a [Single[String]]
      node.ops.head.asInstanceOf[Single[String]]
        .f(intermediateStr) mustBe Success(intermediateStr.substring(4))
      node.result   mustBe Success(testString.substring(4))

      val nextNode = node.resume.left.value()
      nextNode.el   mustBe Success(testString.substring(4))
      nextNode.ops  mustBe empty
    }
    "create multi-op object" in {
      val firstOp = (s: String) => s.substring(3)
      val secondOp = (s: String) => s.charAt(2).toString
      val node = end(testString).map(firstOp).map(secondOp)

      node.el       mustBe Success(testString)
      node.ops.size mustBe 2

      node.ops.head         mustBe a [Single[String]]
      node.ops.head.asInstanceOf[Single[String]]
        .f(intermediateStr) mustBe Success(firstOp(intermediateStr))

      node.ops(1)           mustBe a [Single[String]]
      node.ops(1).asInstanceOf[Single[String]]
        .f(intermediateStr) mustBe Success(secondOp(intermediateStr))

      node.result   mustBe Success(secondOp(firstOp(testString)))

      val nextNode = node.resume.left.value()
      nextNode.el       mustBe Success(firstOp(testString))
      nextNode.ops.size mustBe 1

      nextNode.ops.head     mustBe a [Single[String]]
      nextNode.ops.head.asInstanceOf[Single[String]]
        .f(intermediateStr) mustBe Success(secondOp(intermediateStr))

      val nextNode2 = nextNode.resume.left.value()
      nextNode2.el   mustBe Success(secondOp(firstOp(testString)))
      nextNode2.ops  mustBe empty
    }
    "create flatMap op over object" in {
      val node = end(testString)
        .flatMap(s => end(s + intermediateStr))
        .flatMap(s => end(s + intermediateStr))

      node.ops.size mustBe 2
      node.result   mustBe Success(testString + intermediateStr + intermediateStr)

      val nextNode = node.resume.left.value()
      nextNode.el       mustBe Success(testString + intermediateStr)
      nextNode.ops.size mustBe 1
    }
    "create nested flatMap op over object" in {
      val node = end(testString)
        .flatMap(s => end(s + intermediateStr)
          .flatMap(s => end(s + intermediateStr)))

      node.ops.size mustBe 1
      node.result   mustBe Success(testString + intermediateStr + intermediateStr)

      val nextNode = node.resume.left.value()
      nextNode.el       mustBe Success(testString + intermediateStr)
      nextNode.ops.size mustBe 1
    }
    "run erroneous map op over object" in {
      val node = end(testString).map(_.charAt(20).toString)

      node.el       mustBe Success(testString)
      node.ops.size mustBe 1
      node.result   mustBe a [Failure[StringIndexOutOfBoundsException]]

      val nextNode = node.resume.left.value()
      nextNode.el  mustBe a [Failure[StringIndexOutOfBoundsException]]
      nextNode.ops mustBe empty
    }
    "run erroneous flatMap op over object" in {
      val node = end(testString).flatMap(s => throw failedObj)

      node.el       mustBe Success(testString)
      node.ops.size mustBe 1
      node.result   mustBe a [Failure[EmptyCellException]]

      val nextNode = node.resume.left.value()
      nextNode.el  mustBe a [Failure[EmptyCellException]]
      nextNode.ops mustBe empty
    }
    "pass error through subsequent ops" in {
      val node = end(testString).map(_.charAt(20).toString).map(_ + "testing")

      node.el       mustBe Success(testString)
      node.ops.size mustBe 2
      node.result   mustBe a [Failure[StringIndexOutOfBoundsException]]

      val nextNode = node.resume.left.value()
      nextNode.el       mustBe a [Failure[StringIndexOutOfBoundsException]]
      nextNode.ops.size mustBe 1

      val nextNode2 = nextNode.resume.value
      nextNode2 mustBe a [Failure[StringIndexOutOfBoundsException]]
    }
    "recover error" when {
      "called on end node" in {
        val node =
          stepFail(failedObj)
            .recoverWith { case _: InvalidPuzzleException => end(testString) }

        node.el       mustBe a [Failure[EmptyCellException]]
        node.ops.size mustBe 1
        node.result   mustBe Success(testString)

        val nextNode = node.resume.left.value()
        nextNode.el   mustBe Success(testString)
        nextNode.ops  mustBe empty
      }
      "called on multi-op node" in {
        val node =
          end(testString).map(_.charAt(20).toString)
            .recoverWith { case _: StringIndexOutOfBoundsException => end(errorCaught) }

        node.el       mustBe Success(testString)
        node.ops.size mustBe 2
        node.result   mustBe Success(errorCaught)
      }
      "called on multi-op node and apply following ops" in {
        val node =
          end(testString).map(_.charAt(20).toString)
            .recoverWith { case _: StringIndexOutOfBoundsException => end(errorCaught).map(_.substring(4)) }
            .flatMap(s => end("pre " + s).map(_ + " post"))

        node.el       mustBe Success(testString)
        node.ops.size mustBe 3
        node.result   mustBe Success(s"pre ${errorCaught.substring(4)} post")
      }
      "called on multi-FlatMap node" in {
        val op = (s: String) => s.substring(3)
        val node = end(testString)
          .flatMap(s => end(s.charAt(20).toString))
          .recoverWith { case _: StringIndexOutOfBoundsException => end(errorCaught) }
          .flatMap(s => end(s.substring(3)))

        node.ops.size mustBe 3
        node.result   mustBe Success(errorCaught.substring(3))
      }
    }
//    "not recover error" when {
//      "cause is not defined in partial function" in {
//        val node =
//          stepFail(failedObj)
//            .recover { case _: NoSuchElementException => end(testString) }
//
//        node        mustBe End(Failure(EmptyCellException(Seq.empty, 3)))
//        node.result mustBe Failure(EmptyCellException(Seq.empty, 3))
//        node.resume mustBe Right(Failure(EmptyCellException(Seq.empty, 3)))
//      }
//    }
  }
}
