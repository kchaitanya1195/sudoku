package bruteforce

import bruteforce.Solver.{Puzzle, debug}

import scala.annotation.tailrec

case class Solver(puzzle: Seq[Int]) {
  /**
   * Fetch the greatest index where the puzzle cell is editable
   *
   * @param i the found index must be less than or equal to this
   * @return
   */
  @tailrec
  private def getLastEditableCell(i: Int): Int = {
    if (i < 0)
      i
    else if (puzzle(i) == -1)
      i
    else
      getLastEditableCell(i-1)
  }

  def solve(): Puzzle = {
    @tailrec
    def trSolve(solution: Puzzle, curInd: Int): Puzzle = {
      if (curInd >= 0 && curInd < 81)
        debug(s"$curInd: ${solution(curInd)} ", sameLine = true)
      curInd match {
        case _ if curInd < 0 =>
          // probably invalid puzzle
//          debug("invalid")
          solution
        case _ if curInd >= solution.size =>
          // went past grid size. solved?
          debug("solved")
          solution
        case _ if puzzle(curInd) != -1 =>
          // current cell is question cell. skip it
          debug("question cell")
          trSolve(solution, curInd + 1)
        case _ if solution(curInd) != -1 =>
          // current cell has a tested value
          val r = curInd / 9
          val c = curInd % 9
          val curVal = solution(curInd)
          // search for next valid value
          (curVal + 1 to 9).find(solution.isValidEl(_, r, c)) match {
            case Some(possVal) =>
              // if found, update
              debug(s"tried and updated value to $possVal")
              trSolve(solution.update(r, c, possVal), curInd + 1)
            case None =>
              // else reset to -1 and go to previous cell
              debug("tried and exhausted cell")
              trSolve(solution.update(r, c, -1), getLastEditableCell(curInd - 1))
          }
        case _ =>
          val r = curInd / 9
          val c = curInd % 9

          (1 to 9 ).find(solution.isValidEl(_, r, c)) match {
            case Some(possVal) =>
              // if found, update
              debug(s"updated value to $possVal")
              trSolve(solution.update(r, c, possVal), curInd + 1)
            case None =>
              // else reset to -1 and go to previous cell
              debug("exhausted cell")
              trSolve(solution.update(r, c, -1), getLastEditableCell(curInd - 1))
          }
      }
    }

    trSolve(Puzzle(puzzle), 0)
  }
}

object Solver {
  def getGridIndex(r: Int, c: Int): Int = {
    val gridR = r / 3
    val gridC = c / 3

    3 * gridR + gridC
  }

  def debug(message: String, sameLine: Boolean = false): Unit = {
    if (false) {
      if (sameLine)
        print(message)
      else
        println(message)
    }
  }

  case class Puzzle(puzzle: Seq[Int]) {
    private val rows = puzzle.grouped(9).toSeq
    private val columns = rows.transpose
    private val grids = for {
      rowGrid <- Seq(0 to 2, 3 to 5, 6 to 8)
      colGrid <- Seq(0 to 2, 3 to 5, 6 to 8)
    } yield {
      for {
        r <- rowGrid
        c <- colGrid
      } yield 9 * r + c
    }.map(puzzle(_))

    def isValidEl(el: Int, r: Int, c: Int): Boolean = {
      !rows(r).contains(el) &&
        !columns(c).contains(el) &&
        !grids(getGridIndex(r, c)).contains(el)
    }

    def update(r: Int, c: Int, el: Int): Puzzle =
      Puzzle(puzzle.patch(9 * r + c, Seq(el), 1))

    def size: Int = puzzle.size

    def apply(i: Int): Int = puzzle(i)
  }
}
