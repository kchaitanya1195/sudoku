package bruteforce

import bruteforce.Solver.{Puzzle, debug}

import scala.annotation.tailrec

case class StepSolver(solution: Puzzle, curInd: Int) {
  @tailrec
  private def getLastEditableCell(puzzle: Seq[Int], i: Int): Int = {
    if (i < 0)
      i
    else if (puzzle(i) == -1)
      i
    else
      getLastEditableCell(puzzle, i - 1)
  }

  def step(puzzle: Seq[Int]): StepSolver = {
    if (curInd >= 0 && curInd < 81)
      debug(s"$curInd: ${solution(curInd)} ", sameLine = true)
    else
      debug(s"$curInd: ", sameLine = true)
    curInd match {
      case _ if curInd < 0 =>
        // probably invalid puzzle
        debug("invalid")
        this
      case _ if curInd >= solution.size =>
        // went past grid size. solved?
        debug("solved")
        this
      case _ if puzzle(curInd) != -1 =>
        // current cell is question cell. skip it
        debug("question cell")
        StepSolver(solution, curInd + 1)
      case _ if solution(curInd) != -1 =>
        // current cell has a tested value
        val r = curInd / 9
        val c = curInd % 9
        val curVal = solution(curInd)
        // search for next valid value
        (curVal + 1 to 9).find(solution.isValidEl(_, r, c)) match {
          case Some(possVal) =>
            // if found, update
            debug(s"tried and updated value to $possVal")
            StepSolver(solution.update(r, c, possVal), curInd + 1)
          case None =>
            // else reset to -1 and go to previous cell
            debug("tried and exhausted cell")
            StepSolver(solution.update(r, c, -1), getLastEditableCell(puzzle, curInd - 1))
        }
      case _ =>
        val r = curInd / 9
        val c = curInd % 9

        (1 to 9).find(solution.isValidEl(_, r, c)) match {
          case Some(possVal) =>
            // if found, update
            debug(s"updated value to $possVal")
            StepSolver(solution.update(r, c, possVal), curInd + 1)
          case None =>
            // else reset to -1 and go to previous cell
            debug("exhausted cell")
            StepSolver(solution.update(r, c, -1), getLastEditableCell(puzzle, curInd - 1))
        }
    }
  }
}
