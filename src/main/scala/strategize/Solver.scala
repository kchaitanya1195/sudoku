package strategize

import processing.core.PApplet
import strategize.Utils.*
import strategize.Stepper.*

import scala.util.{Failure, Success}

case class Solver(puzzle: Seq[Int])(using applet: PApplet) {
  val grid: Grid = Seq.fill(81)((1 to 9).toSet)

  private def randomSolver(current: Stepper[Grid]): Stepper[Grid] = {
    current.flatMap { g =>
      if (g.forall(_.size == 1)) {
        // solved
        end(g)
      } else {
        // make a random choice
        val next = g.zipWithIndex.filter(_._1.size != 1)
          .groupBy(_._1.size)
          .toList.minBy(_._1)._2
          .toList.randomEl()
          .map { case (el, i) =>
            lazy val selected = el.toList.randomEl().get
            if (el.nonEmpty) {
              println(s"Selecting $selected from {$el} for index ${getRowColumn(i)}")
              assign(g, i, selected)
                .recoverWith { case e: InvalidPuzzleException =>
                  // if choice causes invalid state
                  println(s"Invalid choice: ${e.getMessage}")
                  eliminate(g, i, selected)
                }
            } else end(g)
          }.get

        randomSolver(next)
      }
    }
  }

  def initialize(): Stepper[Grid] = {
    val initial = puzzle.zipWithIndex.foldLeft(end(grid)) { case (grid, (v, i)) =>
      if (v == -1)
        grid
      else
        grid.flatMap(g => assign(g, i, v))
    }

    randomSolver(initial)
  }

  def solve(): Seq[String] =
    initialize().result.recover {
      case gridEx: InvalidPuzzleException =>
        gridEx.printStackTrace()
        gridEx.grid
    }.map(_.map(_.mkString(","))).get

  /**
   * Assign `digit` to the `square`. This function will actually eliminate all other
   * possible values from the square leaving only `digit`.
   *
   * @param grid    Sudoku puzzle
   * @param square  Index of square in the puzzle
   * @return        Updated puzzle
   */
  def assign(grid: Grid, square: Int, digit: Int): Stepper[Grid] = {
    println(s"Assigning $digit to ${getRowColumn(square)}. To eliminate: ${grid(square).filter(_ != digit)}")
    grid(square).filter(_ != digit)
      .foldLeft(end(grid))((grid2, digit2) =>
        grid2.flatMap(g => eliminate(g, square, digit2)))
  }

  /**
   * Eliminate 'digit' from the possible values for 'square' from 'grid'
   * @param grid    Sudoku puzzle
   * @param square  Index of square in the puzzle
   * @return        Updated puzzle
   */
  def eliminate(grid: Grid, square: Int, digit: Int): Stepper[Grid] = {
    println(s"Eliminating $digit from ${getRowColumn(square)}")
    if (!grid(square).contains(digit))
      end(grid)      // Already eliminated
    else {
      // 1. Eliminate digit
      val updated = grid.updated(square, grid(square) - digit)
      if (updated(square).isEmpty)
        fail(EmptyCellException(updated, square))
      else {
        // 2. If current square has only one possible value, eliminate this value from its peers
        val updated2 = {
          if (updated(square).size == 1) {
            val ans = updated(square).head
            println(s"Square ${getRowColumn(square)} has single possibility: $ans")
            peerMap(square).foldLeft(end(updated))((grid2, peer) =>
              grid2.flatMap(g => eliminate(g, peer, ans)))
          } else end(updated)
        }

        // 3. Find squares in peer units that have only one possible value
        unitMap(square).foldLeft(updated2){ (grid3, unit) =>
          // Find squares in this unit that are possible positions for 'digit'
          grid3.flatMap{ g =>
            val placesForD = unit.filter(g(_) contains digit)
            if (placesForD.isEmpty)
              fail(InvalidUnitException(g, unit, digit))
            else if (placesForD.size == 1) {
              println(s"Only one possible position for $digit in unit ${unitToString(unit)} : ${getRowColumn(placesForD.head)}")
              assign(g, placesForD.head, digit)
            } else end(g)
          }
        }
      }
    }
  }
}

object Solver {
  case class StepSolver(solver: Stepper[Grid]) {
    def step(): StepSolver =
      solver.resume match {
        case Left(stepper) => StepSolver(stepper())
        case Right(grid) =>
          grid match {
            case Failure(gridEx: InvalidPuzzleException) =>
              println("Ending on exception")
              gridEx.printStackTrace()
              StepSolver(end(gridEx.grid))
            case Success(grid) =>
              StepSolver(end(grid))
            case Failure(e) => throw e
          }
      }

    def solution: Seq[String] = solver.el.get.map(_.mkString(","))
  }

  object StepSolver {
    def apply(puzzle: Seq[Int])(using applet: PApplet): StepSolver = new StepSolver(Solver(puzzle).initialize())
  }
}
