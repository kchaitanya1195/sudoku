package strategize

import strategize.Utils.{Grid, InvalidPuzzleException}
import strategize.Stepper.*

import scala.annotation.unused
import scala.util.{Failure, Success, Try}

case class Stepper[E](el: Try[E], ops: List[Step[E]]) {
  def map(f: E => E): Stepper[E] =
    this.copy(ops = ops :+ Single(e => Try(f(e))))

  def flatMap(f: E => Stepper[E]): Stepper[E] =
    this.copy(ops = ops :+ Multi(f))

  def recoverWith(pf: PartialFunction[Throwable, Stepper[E]]): Stepper[E] =
    this.copy(ops = ops :+ RecoverWith(pf))

  def resume: Either[() => Stepper[E], Try[E]] = (el, ops) match {
    case (_, Nil) => Right(el)
    case (Success(e), op :: rest) => op match {
      case Single(f) => Left(() => this.copy(el = f(e), ops = rest))
      case Multi(f) => Left(() =>
        Try(f(e)).fold(
          ex => this.copy(Failure(ex), rest),
          next => next.copy(ops = next.ops ++ rest)
        )
      )
      case RecoverWith(_) => Left(() => this.copy(ops = rest))
    }
    case (Failure(e), op :: rest) => op match {
      case RecoverWith(pf) if pf.isDefinedAt(e) => Left { () =>
        Try(pf(e)).fold(
          ex => this.copy(Failure(ex), rest),
          next => next.copy(ops = next.ops ++ rest)
        )
      }
      case _ => this.copy(ops = rest).resume
    }
  }

  def result: Try[E] = (el, ops) match {
    case (_, Nil) => el
    case (Success(e), op :: rest) => op match {
      case Single(f) => this.copy(el = f(e), ops = rest).result
      case Multi(f) =>
        Try(f(e)).fold(
          ex => this.copy(Failure(ex), rest).result,
          next => next.copy(ops = next.ops ++ rest).result
        )
      case RecoverWith(_) => this.copy(ops = rest).result
    }
    case (Failure(e), op :: rest) => op match {
      case RecoverWith(pf) if pf.isDefinedAt(e) =>
        Try(pf(e)).fold(
          ex => this.copy(Failure(ex), rest).result,
          next => next.copy(ops = next.ops ++ rest).result
        )
      case _ => this.copy(ops = rest).result
    }
  }
}

object Stepper {
  sealed abstract trait Step[E]
  case class Single[E](f: E => Try[E]) extends Step[E]
  case class Multi[E](f: E => Stepper[E]) extends Step[E]
  case class RecoverWith[E](pf: PartialFunction[Throwable, Stepper[E]]) extends Step[E]

  def end[E](el: => E): Stepper[E] = new Stepper(Try(el), List.empty)
  def fail[E](e: Throwable): Stepper[E] = new Stepper(Failure(e), List.empty)
}
