package strategize

import processing.core.PApplet

object Utils {
  class InvalidPuzzleException(val grid: Grid, errorMsg: String) extends Exception(errorMsg)
  case class EmptyCellException(override val grid: Grid, cellInd: Int)
    extends InvalidPuzzleException(grid, s"No possible values for cel ${getRowColumn(cellInd)}")
  case class InvalidUnitException(override val grid: Grid, unit: Set[Int], digit: Int)
    extends InvalidPuzzleException(grid, s"No possible places for digit $digit in ${unit.map(getRowColumn)}")

  type Grid = Seq[Set[Int]]

  extension[A] (ls: List[A]) {
    def randomEl()(using applet: PApplet): Option[A] = ls.lift(applet.random(0, ls.size.toFloat).toInt)
  }

  def getRowColumn(ind: Int): (Int, Int) = ((ind / 9) + 1, (ind % 9) + 1)
  def getInd(r: Int, c: Int): Int = r * 9 + c
  def unitToString(unit: Set[Int]): String = unit.map(getRowColumn).mkString(",")

  val indexes: Seq[Int] = 0 to 80
  // List of all possible rows, cols and boxes
  val unitList: Set[Set[Int]] = {
    val rows = indexes.grouped(9).toSeq
    val cols = rows.transpose
    val grids = (for {
      rowGrid <- Seq(0 to 2, 3 to 5, 6 to 8)
      colGrid <- Seq(0 to 2, 3 to 5, 6 to 8)
    } yield {
      for {
        r <- rowGrid
        c <- colGrid
      } yield 9 * r + c
    }.toSet).toSet

    rows.map(_.toSet).toSet ++ cols.map(_.toSet).toSet ++ grids
  }
  // Map of a square to list of units that contain that square
  val unitMap: Map[Int, Set[Set[Int]]] = indexes.map(i =>
    i -> unitList.filter(_ contains i)
  ).toMap
  // Map of square to list of squares that are in the same unit
  val peerMap: Map[Int, Set[Int]] = unitMap.map { case (i, units) =>
    i -> (units.flatten - i)
  }
}
