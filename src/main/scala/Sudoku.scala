import processing.core.{PApplet, PConstants}
import strategize.Solver.StepSolver

import scala.util.Try

class Sudoku extends PApplet {
  private given applet: Sudoku = this
  private val PADDING = 40

//  val puzzle = Seq(
//    -1, -1, -1, -1, -1, -1, -1, -1, -1,
//    -1, -1, -1,  2, -1,  7,  8, -1,  1,
//    -1, -1,  7, -1,  1,  3, -1, -1, -1,
//    -1,  9, -1, -1, -1,  2, -1,  5,  7,
//    -1, -1, -1,  7,  4, -1, -1, -1,  6,
//    -1,  5, -1,  1, -1, -1,  2, -1, -1,
//    -1, -1, -1, -1, -1, -1, -1, -1,  5,
//     1,  8, -1,  4,  2, -1, -1,  6, -1,
//    -1, -1, -1, -1,  6, -1,  9, -1, -1
//  )
  val puzzle: Seq[Int] = Seq(
    -1, -1, -1, -1, -1, -1, -1, -1,  9,
    -1, -1,  9, -1, -1, -1, -1, -1,  1,
     5, -1, -1, -1, -1,  4, -1, -1, -1,
    -1, -1, -1,  8,  3, -1,  1, -1, -1,
    -1,  1, -1,  7, -1, -1,  3, -1, -1,
     7, -1, -1, -1,  9,  6, -1, -1, -1,
    -1,  6, -1, -1,  7, -1,  4, -1, -1,
     3,  7, -1,  5, -1, -1, -1, -1,  8,
    -1,  8, -1, -1, -1, -1, -1,  6, -1
  )
  // private val solver = Solver(puzzle)
  private var solver = StepSolver(puzzle)

  override def settings(): Unit = {
    // noLoop()
    randomSeed(123456789)
    size(720, 720)
  }

  override def draw(): Unit = {
    background(0xFFFFFFFF)

    val gridLength = (width - PADDING * 2) / 9 * 9
    val cellLength = gridLength / 9

    // Drawing grid
    stroke(0)
    (0 to 9).foreach { i =>
      // Calculate box borders
      strokeWeight( if (i % 3 == 0) 3 else 1 )
      // Vertical lines
      line(PADDING.toFloat + i * cellLength, PADDING.toFloat,
        PADDING.toFloat + i * cellLength, PADDING.toFloat + gridLength)
      // Horizontal lines
      line(PADDING.toFloat, PADDING.toFloat + i * cellLength,
        PADDING.toFloat + gridLength, PADDING.toFloat + i * cellLength)
    }

    textSize(3.toFloat * cellLength / 5)
    textAlign(PConstants.CENTER, PConstants.CENTER)
    (1 to 10).foreach(_ => solver = solver.step())
    // solver = solver.step()
    // val solved = solver.solve()
    for {
      rowNum <- 0 to 8
      colNum <- 0 to 8
    } yield {
      val curInd = 9 * rowNum + colNum

      // Color question digits differently
      if (puzzle(9 * rowNum + colNum) == -1)
        fill(0xFF0000FF)
      else
        fill(0)

      if (solver.solution(curInd) contains ",")
        textSize(cellLength.toFloat / 10)
      else
        textSize(3.toFloat * cellLength / 5)

      text(solver.solution(curInd), PADDING.toFloat + colNum * cellLength, PADDING.toFloat + rowNum * cellLength,
        cellLength.toFloat, cellLength.toFloat)
    }
  }

  override def keyPressed(): Unit = {
    key match {
      case ' ' =>
        if (isLooping) noLoop()
        else loop()
      case PConstants.ENTER | PConstants.RETURN =>
        frameCount += 1
        redraw()
      case PConstants.TAB =>
        solver = StepSolver(puzzle)
        loop()
      case 's' =>
        // val frameName = s"sudoku-${System.currentTimeMillis()}.png"
        val frameName = s"sudoku-debug.png"
        saveFrame(frameName)
      case _ =>
    }
  }
}

object Sudoku extends App {
  PApplet.main("Sudoku")
}
