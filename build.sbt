name := "sudoku"

version := "0.1"

scalaVersion := "3.1.3"
scalacOptions ++= Seq("-deprecation")

libraryDependencies += "org.processing" % "core" % "3.3.7"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.13" % "test"
